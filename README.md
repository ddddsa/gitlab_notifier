Оглавление:

* [GitLab notifier](#gitlab-notifier)
* [Workflow](#workflow)
* [Config](#config)
* [Usage](#usage)
    * [Locally](#locally)
    * [For real](#for-real)

# GitLab notifier

> Script works with python 2.7\3.4+

This script allows you to 'subscribe' to changes in specific files in any GitLab repository. Script will send notifications to subscribed users if any of the specified files are changed after push (to specific branch, optionally).

# Workflow

There are 2 ways to make this script work:

**(recommended) Forking this repository**

1. Fork this repository.
2. Update `config.yaml` in forked repository to track your projects.
3. Add section to the `.gitlab-ci.yml` of the tracked projects ([example](#for-real)), which will run this script every time somebody pushes into `develop` branch, for example.
4. If you need to change configuration of any project, you just update `config.yaml` in this repository and changes will be automatically applied for all your set up projects, no need to push anything else to them.

**Using local configs**

1. Create a `config.yaml` file with list of files you want to track in specific project.
2. Add this config into the tracked project.
3. Add section to the `.gitlab-ci.yml` of the tracked project ([example](#for-real)), which will run this script every time somebody pushes into `develop` branch, for example. This time use `--config -c` option to force script to use local config.

# Config

By default script uses config-file which is located in this very project. It's recommended to fork this repository and update the config-file. This way you will have all the options for all your projects in one place.

**mail_config**

```yaml
mail_config:
    host: mail.example.com
    port: 587
    user: notifier@example.com
    password: currently_stored_as_plain_text
```

Credentials of the mail server account which will send the notifications.

**projects**

```yaml
projects:
    test-project:
        - match:
            - test1.txt
            - test/file1.txt
          mailing_list:
            - mail1@gmail.com
            - mail2@gmail.com
        - mailing_list:
            - mail2@gmail.com
          match:
            - test1.txt
            - test/*
          ignore:
            - file3.txt
```

This section describes GitLab-projects which are being tracked.

`test-project` — name of the project. That one which you see in your browser address bar: gitlab.com/username/**test-project**.

Next goes a list with tracking settings. Each group can have following sections:

`mailing_list` — list of email-addresses, which will receive notifications.

`match` — list of [glob](https://en.wikipedia.org/wiki/Glob_(programming))-like patterns. If files in the tracked repository, which match these patterns, are changed, script will send notifications to the mailing_list, set up in this group.

`ignore` — list of glob-like patterns of files to ignore.

For example, if you want to subscribe to changes in all files in the `contrib/src` folder except `license.txt`, you can use following patterns:

```yaml
match:
  - contrib/src/*
ignore:
  - license.txt
```

> Recursive patterns are not supported. Add each directory separately

# Usage

Script must be run in the root of Git repository. To check usage add the `-h --help` option:

```bash
$ git clone https://gitlab.com/ddddsa/gitlab_notifier.git
$ python ./gitlab_notifier/gitlab_notifier.py -h
```

Script can work in two modes:

* local — for experiments and testing;
* Inside GitLab CI — primary usage.

Script works a bit differently in each mode.

## Locally

To turn use the local mode, just specify the `-p --project` option — name of the GitLab-project, which is being tested.

Go into the local Git repository folder:

```bash
$ cd ~/Projects/important_app_repo/
$ # Note, that important_app_repo should already be added to the script config
$ git clone https://gitlab.com/ddddsa/gitlab_notifier.git
```

Install dependencies:

```bash
$ pip install -r ./gitlab_notifier/requirements.txt
```

Launch script with current project name in the `-p` option:

```bash
$ python ./gitlab_notifier/gitlab_notifier.py -p important_app_repo
Project name specified. Running local simplified version of the script.
Got list of changed_files:
['test/file1.txt', 'test/file3.txt']
Some of the tracked files were changed in the project test-project:
 test/file1.txt
Sending notifications to: mail1@gmail.com, mail2@gmail.com
Notification sent
```

Without any other parameters script compares current directory state with the last commit. In the example above script tells us that two files had changed: `test/file1.txt` и `test/file3.txt`. First of these two is tracked by mail1@gmail.com and mail2@restream.rt.ru, so the script sends them notifications.

**Specifying commit hashes**

You can specify hashes of two commits which you want to compare:

```bash
$ python ./gitlab_notifier/gitlab_notifier.py -p important_app_repo e56f81816349 5eea499882cf
Project name specified. Running local simplified version of the script.
Got list of changed_files:
['.gitlab-ci.yml', 'test/file1.txt', 'test/file2.txt', 'test/file3.txt', 'test1.txt', 'test2.txt', 'test3.txt']
Some of the tracked files were changed in the project test-project:
 test/file1.txt
test1.txt
Sending notifications to: mail1@gmail.com, mail2@gmail.com
Notification sent
Some of the tracked files were changed in the project test-project:
 test/file2.txt
test2.txt
Sending notifications to: mail1@gmail.com
Notification sent
```

Also you can specify only one hash. Script works the same as the `git diff --name-only` command:

* **No commits are specified** — compares current dir state with latest commit.
* **One hash specified** — compares current dir state with the specified commit.
* **Two commit hashes specified** — compares two commits.

> Commit hashes can be specified for testing, they work only in local mode. Under GitLab CI commit hashes are ignored.

**Testing script without sending mail**

If you want to play with the script but don't want any mail to be sent, use the `-q --quiet` option:

```bash
$ python3 ./gitlab_notifier/gitlab_notifier.py -p important_app_repo -q
Project name specified. Running local simplified version of the script.
Got list of changed_files:
['test/file1.txt', 'test/file3.txt']
Some of the tracked files were changed in the project test-project:
 test/file1.txt
Would notify:
mail1@gmail.com
mail2@gmail.com
```

At the end script tells us which addresses it would notify if there wasn't `-q` option.

## For real

To subscribe to changes in specific files in any GitLab project, first update the [config](#config) file, either in the forked repository, or the local one, which will be added to the tracked project.

Then you'll have to add a [GitLab CI](https://docs.gitlab.com/ee/ci/) job which will run this script after push to specific branch.

Example:

```yaml
# .gitlab-ci.yaml

notifier:
  stage: notify
  allow_failure: true
  script:
    - git clone https://gitlab.com/ddddsa/gitlab_notifier.git
    - pip install -r ./gitlab_notifier/requirements.txt
    - python ./gitlab_notifier/gitlab_notifier.py
  only:
    - master

```

`allow_failure: true` — allow this job to fail.

`- git clone https://gitlab.com/ddddsa/gitlab_notifier.git` — cloning the script repo.

`- pip install -r ./gitlab_notifier/requirements.txt` — installing requirements.

`- python ./gitlab_notifier/gitlab_notifier.py` — running script. If you decided not to fork but use the original repo, then also supply the `--config -c` option.

Limiting the job to be run only after commits to master.

```
only:
  - master
```

Without this section script will send letters after pushing to *any* branch!