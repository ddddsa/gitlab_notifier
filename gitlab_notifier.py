# -*- coding: utf-8 -*-

import sys

if __name__ == '__main__':
    if sys.version[0] == '3':
        from src.py3.main import run_command
    else:
        from src.py2.main import run_command
    run_command()
