# -*- coding: utf-8 -*-

'''classes for sending notifications mails'''

from smtplib import SMTP
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class SimplePostman(object):
    message = '''Subject: Gitlab notification
Some of the tracked files were changed in the project %s:\n %s'''

    def __init__(self, host, port, user, password, project):
        self.smtp = SMTP(host=host, port=port)
        self.smtp.starttls()
        self.smtp.login(user, password)
        self.project = project

    def notify(self, file_list, email_list):
        message = self.construct_message(file_list)
        self.smtp.sendmail('gitlab_notyfier', email_list, message)
        print 'Notification sent'

    def construct_message(self, file_list):
        return self.message % (self.project, '\n'.join(file_list))


class CIPostman(SimplePostman):
    def __init__(self, host, port, user, password, ci_commander):
        super(CIPostman, self).__init__(host, port, user, password, '_')
        self.ci_commander = ci_commander

    def notify(self, file_list, email_list):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Gitlab notification"
        msg['From'] = 'gitlab_notyfier'
        msg['To'] = ', '.join(email_list)
        message = MIMEText(self.construct_message(file_list), 'html', 'utf-8')
        msg.attach(message)
        self.smtp.sendmail('gitlab_notyfier', email_list, msg.as_string())
        print 'Notification sent'

    def construct_message(self, file_list):
        message = \
'''<p>In the project <a href="{repo_url}">{project}</a>, to which you are subscribed, changed some of the files, which you marked.</p>

<p>Branch: <a href="{branch_url}">{branch}</a>.<br/>
Commit: <a href="{commit_url}">{commit_title}</a> ({commit_sha}).</p>

<p>Changed files:</p>

<p>{formatted_files}</p>

<p><a href="{full_diff}">Look at all pushed changes</a></p>
'''
        kwargs = {}

        kwargs['repo_url'] = self.ci_commander.get_repo_url()
        kwargs['commit_sha'] = self.ci_commander.get_current_commit_sha()
        kwargs['commit_title'] = self.ci_commander.get_current_commit_title()
        kwargs['project'] = self.ci_commander.get_project_name()
        kwargs['branch'] = self.ci_commander.get_branch()

        kwargs['branch_url'] = '/'.join((kwargs['repo_url'],
                                         'tree',
                                         kwargs['branch']))
        kwargs['commit_url'] = '/'.join((kwargs['repo_url'],
                                         'commit',
                                         kwargs['commit_sha']))
        prev_commit = self.ci_commander.get_previous_commit_sha()
        kwargs['full_diff'] = kwargs['repo_url'] + '/' + 'compare/' + \
            prev_commit + '...' + kwargs['commit_sha']

        kwargs['formatted_files'] = ''
        for file in file_list:
            file_url = '/'.join((kwargs['repo_url'],
                                 'blob',
                                 kwargs['branch'],
                                 file))
            add = '<a href="{file_url}">{file}</a><br/>\n'.format(file_url=file_url,
                                                                  file=file)
            kwargs['formatted_files'] += add

        return message.format(**kwargs)
