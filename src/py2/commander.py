# -*- coding: utf-8 -*-

'''classes related to running os commands'''


import re
from subprocess import Popen, PIPE


CI_COMMIT_BEFORE_SHA = '"$CI_COMMIT_BEFORE_SHA"'
CI_BUILD_REF = '"$CI_BUILD_REF"'
CI_COMMIT_REF_NAME = '"$CI_COMMIT_REF_NAME"'
CI_COMMIT_SHA = '"$CI_COMMIT_SHA"'
CI_REPOSITORY_URL = '"$CI_REPOSITORY_URL"'
CI = '"$CI"'
CI_PROJECT_NAME = '"$CI_PROJECT_NAME"'
CI_COMMIT_TITLE = '"$CI_COMMIT_TITLE"'


class NotAGitRepository(Exception):
    pass


class NotRunInCI(Exception):
    pass


class GitCommander(object):

    def __init__(self):
        if not self.check_git():
            raise NotAGitRepository('Not a git repository!')

    def try_run(self, command, caller='executing command'):
        '''
        Try and run command. If command fails: print stderror with addition
        of 'caller' text.
        Return decoded stdout anyway.
        '''
        proc = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
        c, e = proc.communicate()
        if proc.returncode != 0:
            message = 'Commander failed while {caller}:\n$ {command}\n{err}'
            print message.format(caller=caller,
                                 err=e.decode('utf8'),
                                 command=command)
        return c.decode('utf8')

    def try_echo(self, var):
        return self.try_run('echo ' + var, 'getting ' + var).strip()

    def check_git(self):
        '''True if current folder is a git repository. False otherwise'''
        proc = Popen(['git', 'status'], stdout=PIPE, stderr=PIPE)
        _, e = proc.communicate()
        if proc.returncode == 128:  # not a git repository
            return False
        elif proc.returncode == 0:
            return True
        else:
            raise RuntimeError(e.decode('utf8'))

    def get_changed_list(self,
                         commit1=CI_COMMIT_BEFORE_SHA,
                         commit2=CI_BUILD_REF):
        '''
        Return list of files changed between two commits.and
        If commit2 is falsy, compares to current state.
        Default values will only work in GitLab-CI environment.
        '''
        args = ['git', 'diff', '--name-only']
        if commit1:
            args.append(commit1)
            if commit2:
                args.append(commit2)
        command = ' '.join(args)
        r = self.try_run(command, 'getting list of changed files')
        r = r.strip()
        return r.split('\n') if r else []


class GitLabCICommander(GitCommander):

    def __init__(self):
        super(GitLabCICommander, self).__init__()
        if not self.check_gitlab_ci():
            raise NotRunInCI('Not a GitLab CI environment!')

    def check_gitlab_ci(self):
        r = self.try_echo(CI)
        return r.strip() == 'true'

    def get_repo_url(self):
        pattern = r'gitlab-ci-token:.+@(.+)\.git'
        full = self.try_echo(CI_REPOSITORY_URL)
        match = re.search(pattern, full)
        if match:
            url = match.group(1)
            return 'https://' + url
        else:
            return ''

    def get_branch(self):
        return self.try_echo(CI_COMMIT_REF_NAME)

    def get_current_commit_sha(self):
        return self.try_echo(CI_COMMIT_SHA)

    def get_current_commit_title(self):
        return self.try_echo(CI_COMMIT_TITLE)

    def get_previous_commit_sha(self):
        return self.try_echo(CI_COMMIT_BEFORE_SHA)

    def get_project_name(self):
        return self.try_echo(CI_PROJECT_NAME)
