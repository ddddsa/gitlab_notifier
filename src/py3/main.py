import os
import sys
from optparse import OptionParser

from .commander import (GitCommander, GitLabCICommander, NotAGitRepository,
                        NotRunInCI)
from .notifier import Notifier


def main_ci(config='config.yaml', notify=True):
    '''GitLab CI version of the script'''
    try:
        c = GitLabCICommander()
    except NotAGitRepository:
        print('The script should be run in the root of a Git repository. Exiting')
        return
    except NotRunInCI:
        print('The script should be run by GitLab runner in CI environment. Exiting')
        return
    changed_files = c.get_changed_list()
    if changed_files:
        print('Got list of changed_files: ', changed_files, sep="\n")
        project = c.get_project_name()
        notifier = Notifier(config, project, ci_commander=c)
        notifier.check_files(changed_files, notify)


def main(project, commit1=None, commit2=None, config='config.yaml', notify=True):
    '''local version of the script'''
    try:
        c = GitCommander()
    except NotAGitRepository:
        print('The script should be run in the root of a Git repository. Exiting')
        return
    changed_files = c.get_changed_list(commit1, commit2)
    if changed_files:
        print('Got list of changed_files: ', changed_files, sep="\n")
        notifier = Notifier(config, project)
        notifier.check_files(changed_files, notify)


def run_command():
    usage = \
'''%prog [options] [commit1 [commit2]]

GitLab CI:
To use GitLab CI mode, DON'T specify the --project option.

commit1 and commit2 args are ignored. Compares changed files between previous and current commit.

Local:
To use local mode, specify the --project option.

Works just as git diff --names-only command:
If no commits specified: compares HEAD with current folder state.
If just commit1 specified: compares commit1 with current folder state.
If both commit1 and commit2 specified: compares commit1 and commit2 states.

'''
    opts = OptionParser(usage)
    root = os.path.dirname(sys.argv[0])
    opts.add_option('-c',
                    '--config',
                    dest='config_path',
                    help='PATH to config file',
                    metavar='PATH',
                    default=os.path.join(root, 'config.yaml'))
    opts.add_option('-p',
                    '--project',
                    dest='project',
                    help='Project name for local testing. If not specified — relies on GitLab CI',
                    default='')
    opts.add_option('-q',
                    '--quiet',
                    dest='notify',
                    action='store_false',
                    help="Don't notify anybody by email, just run script and see results",
                    default=True)
    options, args = opts.parse_args()
    config = options.config_path
    if options.project:
        print('Project name specified. Running local simplified version of the script.')
        project = options.project
        commit1 = args[0] if len(args) > 0 else None
        commit2 = args[1] if len(args) > 1 else None
        main(project, commit1, commit2, config, options.notify)
    else:
        print('Running GitLab CI version of the script.')
        main_ci(config, options.notify)
