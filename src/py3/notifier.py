'''the main Notifier class'''

import yaml
from pathlib import Path

from .postman import SimplePostman, CIPostman


class Notifier:
    def __init__(self, config, project_name, ci_commander=None):
        with open(config, 'r') as file:
            config = yaml.load(file.read())
        if 'mail_config' not in config:
            print('Mail server is not configured, notifications are disabled!')
            self.postman = None
        else:
            if ci_commander:
                self.postman = CIPostman(host=config['mail_config']['host'],
                                         port=config['mail_config']['port'],
                                         user=config['mail_config']['user'],
                                         password=config['mail_config']['password'],
                                         ci_commander=ci_commander)
            else:
                self.postman = SimplePostman(host=config['mail_config']['host'],
                                             port=config['mail_config']['port'],
                                             user=config['mail_config']['user'],
                                             password=config['mail_config']['password'],
                                             project=project_name)
        if project_name not in config.get('projects', {}):
            raise RuntimeError('Project %s is missing in config' %
                               project_name)
        else:
            self.config = config['projects'][project_name]
            self.project_name = project_name
            self._listify_config()

    def _listify_config(self):
        """Convert certain sections to lists in case user forgot to put '-'"""
        if type(self.config) is dict:
            self.config = [self.config]
        for group_dict in self.config:
            if 'match' in group_dict:
                if type(group_dict['match']) is str:
                    group_dict['match'] = [group_dict['match']]
            if 'ignore' in group_dict:
                if type(group_dict['ignore']) is str:
                    group_dict['ignore'] = [group_dict['ignore']]
            if 'mailing_list' in group_dict:
                if type(group_dict['mailing_list']) is str:
                    group_dict['mailing_list'] = [group_dict['mailing_list']]

    def check_group(self, group, file_list, notify):
        changed = collect_changed(group.get('match', []),
                                  group.get('ignore', []),
                                  file_list)
        if changed:
            print('Some of the tracked files were changed in the '
                  'project %s:\n %s' % (self.project_name,
                                        '\n'.join(changed)))
            if not notify and group['mailing_list']:
                print('Would notify:', *group['mailing_list'], sep='\n')
            if notify and group['mailing_list'] and self.postman:
                print('Sending notifications to: %s'
                      % ', '.join(group['mailing_list']))
                self.postman.notify(changed,
                                    group['mailing_list'])

    def check_files(self, file_list, notify=True):
        for group in self.config:
            self.check_group(group, file_list, notify)


def collect_changed(match_patterns, ignore_patterns, file_list):
    '''
    Collect a list of files from file_list which match at least one of the
    match_patterns; kick away from it all files which match at leas one of
    ignore_patterns.
    Returns the resulting list.
    '''
    result = []
    if not match_patterns:
        return result
    for f in file_list:
        for p in match_patterns:
            if Path(f).match(p) and f not in result:
                result.append(f)
    if ignore_patterns:
        result = [f for f in result
                  for ip in ignore_patterns
                  if not Path(f).match(ip)]
    return result
